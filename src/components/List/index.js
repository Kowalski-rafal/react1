import React from 'react'


const List = (props) => {
    return (
        <div>
            <ul>{props.list}</ul>
        </div>
    )
}

// class List extends React.Component {
//     render(){
//         return(
//             <div>
//                 <ul>{this.props.list}</ul>
//             </div>
//         )
//     }
// }

export default List;