import React from 'react'
import List from "../../components/List";


const myFavoriteFoods = ['Sushi', 'Szarpana Wieprzowina', 'Burgery', 'Dania kuchni Wietnamskiej', 'Kimchi'];
const listItem = myFavoriteFoods.map((myFavoriteFoods) =>
    <li>{myFavoriteFoods}</li>
);

const homepage = () => {
    return (
        <div>
            <List list = {listItem} />
        </div>
    )
}



export default homepage;